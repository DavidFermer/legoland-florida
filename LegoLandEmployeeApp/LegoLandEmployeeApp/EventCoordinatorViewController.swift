//
//  EventCoordinatorViewController.swift
//  LegoLandEmployeeApp
//
//  Created by David Fermer on 9/10/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse
import AVFoundation

//MARK: - UIPickerViewDataSource
extension EventCoordinatorViewController: UIPickerViewDataSource {
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return 5
	}
	
}

//MARK: - UIPickerViewDelegate
extension EventCoordinatorViewController: UIPickerViewDelegate {
	func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
		return showNames[row]
	}
}

//MARK: - EventCoordinatorViewController
class EventCoordinatorViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
	@IBOutlet weak var eventPicker: UIPickerView!
	//Outlets for first show
	@IBOutlet weak var firstShowHour: UITextField!
	@IBOutlet weak var firstShowMinute: UITextField!
	@IBOutlet weak var firstShowSegmentedControl: UISegmentedControl!
	//Outlets for second show
	@IBOutlet weak var secondShowHour: UITextField!
	@IBOutlet weak var secondShowMinute: UITextField!
	@IBOutlet weak var secondShowSegmentedControl: UISegmentedControl!
	//Outlets for third show
	@IBOutlet weak var thirdShowHour: UITextField!
	@IBOutlet weak var thirdShowMinute: UITextField!
	@IBOutlet weak var thirdShowSegementedControl: UISegmentedControl!
	//OUtlets for fourth show
	@IBOutlet weak var fourthShowHour: UITextField!
	@IBOutlet weak var fourthShowMinute: UITextField!
	@IBOutlet weak var fourthShowSegmentedControl: UISegmentedControl!
	
	//Activitiy Indicator
	@IBOutlet weak var updateActivityIndicator: UIActivityIndicatorView!
	
	//Audio Player to be used to play alert sounds
	var audioPlayer = AVAudioPlayer()
	
	//Variables
	var userName : String?
	
	//These variables will contain the strings that will be uploaded to parse for the selected show.
	var firstShowTime : String?
	var secondShowTime : String?
	var thirdShowTime : String?
	var fourthShowTime : String?
	
	//These varriables will be used to capture the show times in Integer format.  To be used later to complete validity checks.
	var firstShowHourInt: Int?
	var firstShowMinInt: Int?
	var secondShowHourInt: Int?
	var secondShowMinInt: Int?
	var thirdShowHourInt: Int?
	var thirdShowMinInt: Int?
	var fourthShowHourInt: Int?
	var fourthShowMinInt: Int?
	
	//The shows that can be selected.  Assign to picker view.
	let showNames = [
		"Battle for Brickbeard's Bounty",
		"LEGO® Legends of Chima",
		"A Clutch Powers 4-D Adventure",
		"Spellbreaker 4D",
		"Big Test"
	]
	
	//The Keys for each Show Name
	let keysForShows = [
		"m8Lg37uOOM",
		"ZwKquzFbwq",
		"e4kMYDU5dT",
		"m23y2kd3bF",
		"LFsQwU6SEC"
	]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		//eventPicker
		// Do any additional setup after loading the view.
		updateActivityIndicator.hidesWhenStopped = true
		
		//Tap Guesture Recognizer
		var tapGuesture = UITapGestureRecognizer(target: self, action: Selector("handleTapGuestre:"))
		view.addGestureRecognizer(tapGuesture)
	}
	
	
	func handleTapGuestre(tapGuesture: UITapGestureRecognizer) {
		self.view.endEditing(true)
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	

	
	//This Fuction will be used to check the format of all hour fields to ensure they are between 1 and 12.
	func hourCheck (hour: Int) -> Bool {
		switch hour {
		case 1...12: return true
		default: return false
		}
	}
	
	func minCheck (min: Int) -> Bool {
		switch min {
		case 0...59: return true
		default: return false
		}
	}
	
	
	//This function takes a correctly formated hour Int and the am/pm toggle switch and creates an hour between 0 and 23.  This is utilized to help determine if show times are an hour or more apart.
	func create24HourInt (hour: Int, amPm: Int) -> Int {
		if amPm == 0 && hour != 12 {
			return hour
		} else if amPm == 0 && hour == 12 {
			return 0
		} else if amPm == 1 && hour != 12 {
			return hour + 12
		} else {
			return hour
		}
	}
	
	
	func createAlert(errorMessage: String) {
		var alert = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
		presentViewController(alert, animated: true, completion: nil)
		alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
		updateActivityIndicator.stopAnimating()
	}
	
	@IBAction func segmentedControllerActions(sender: UISegmentedControl) {
		switch sender.tag {
		case 0 :
			if sender.selectedSegmentIndex == 2 {
				firstShowHour.hidden = true
				firstShowMinute.hidden = true
				secondShowHour.hidden = true
				secondShowMinute.hidden = true
				secondShowSegmentedControl.selectedSegmentIndex = 2
				secondShowSegmentedControl.hidden = true
				thirdShowHour.hidden = true
				thirdShowMinute.hidden = true
				thirdShowSegementedControl.selectedSegmentIndex = 2
				thirdShowSegementedControl.hidden = true
				fourthShowHour.hidden = true
				fourthShowMinute.hidden = true
				fourthShowSegmentedControl.selectedSegmentIndex = 2
				fourthShowSegmentedControl.hidden = true
			} else {
				firstShowHour.hidden = false
				firstShowMinute.hidden = false
				secondShowSegmentedControl.hidden = false
			}
		case 1 :
			if sender.selectedSegmentIndex == 2 {
				secondShowHour.hidden = true
				secondShowMinute.hidden = true
				thirdShowHour.hidden = true
				thirdShowMinute.hidden = true
				thirdShowSegementedControl.selectedSegmentIndex = 2
				thirdShowSegementedControl.hidden = true
				fourthShowHour.hidden = true
				fourthShowMinute.hidden = true
				fourthShowSegmentedControl.selectedSegmentIndex = 2
				fourthShowSegmentedControl.hidden = true
			} else {
				secondShowHour.hidden = false
				secondShowMinute.hidden = false
				thirdShowSegementedControl.hidden = false
			}
		case 2 :
			if sender.selectedSegmentIndex == 2 {
				thirdShowHour.hidden = true
				thirdShowMinute.hidden = true
				fourthShowHour.hidden = true
				fourthShowMinute.hidden = true
				fourthShowSegmentedControl.selectedSegmentIndex = 2
				fourthShowSegmentedControl.hidden = true
			} else {
				thirdShowHour.hidden = false
				thirdShowMinute.hidden = false
				fourthShowSegmentedControl.hidden = false
			}
		default:
			if sender.selectedSegmentIndex == 2 {
				fourthShowHour.hidden = true
				fourthShowMinute.hidden = true
			} else {
				fourthShowHour.hidden = false
				fourthShowMinute.hidden = false
			}
		}
	}
	
	@IBAction func updateButton(sender: UIButton) {
		updateActivityIndicator.startAnimating()
		//Capture AM/PM status to be used later to turn hours into 24 hour times.  0 to 23 hours.
		var allShowsSegmentIndexArray = [
			firstShowSegmentedControl.selectedSegmentIndex,
			secondShowSegmentedControl.selectedSegmentIndex,
			thirdShowSegementedControl.selectedSegmentIndex,
			fourthShowSegmentedControl.selectedSegmentIndex
		]
		
		
		//This variable is used to build an error message that can be displayed to the user when the click the update button.
		var errorMessage = ""
		
		//Assign values in text fields to thier Ints for validity checking.
		firstShowHourInt = firstShowHour.text.toInt()
		firstShowMinInt = firstShowMinute.text.toInt()
		secondShowHourInt = secondShowHour.text.toInt()
		secondShowMinInt = secondShowMinute.text.toInt()
		thirdShowHourInt = thirdShowHour.text.toInt()
		thirdShowMinInt = thirdShowMinute.text.toInt()
		fourthShowHourInt = fourthShowHour.text.toInt()
		fourthShowMinInt = fourthShowMinute.text.toInt()
		
		//Put hours and Mins into separate arrays so validity checks can be done in loops.
		let showHours = [firstShowHourInt, secondShowHourInt, thirdShowHourInt, fourthShowHourInt]
		let showMins = [firstShowMinInt, secondShowMinInt, thirdShowMinInt, fourthShowMinInt]
		
		//used to hold 24 hour formated hours
		var hour24s = [24,24,24,24]
		
		//Begin Checking validity of hours
		var hourCounter = 0
		//error message handling:
		
		var hourNotFilledInErrorStatus = false
		var hourCheckSuccessErrorStatus = false
		var hour1HourApartErrorStatus = false
		
		for hour in showHours {
			if let hour = hour {
				
				//check hour to ensure it falls between 1 and 12.
				var hourCheckSuccess = hourCheck(hour)
				let hourCheckSuccessErrorMessage = "All Hour Fields must be from 1 to 12."
				if hourCheckSuccess {
					hour24s[hourCounter] = create24HourInt(hour, amPm: allShowsSegmentIndexArray[hourCounter])
					switch hourCounter {
					case 0 :
						firstShowHour.backgroundColor = nil
					case 1 :
						secondShowHour.backgroundColor = nil
					case 2 :
						thirdShowHour.backgroundColor = nil
					default :
						fourthShowHour.backgroundColor = nil
					}
				} else if allShowsSegmentIndexArray[hourCounter] != 2{
					switch hourCounter {
					case 0 :
						firstShowHour.backgroundColor = UIColor.redColor()
					case 1 :
						secondShowHour.backgroundColor = UIColor.redColor()
					case 2 :
						thirdShowHour.backgroundColor = UIColor.redColor()
					default :
						fourthShowHour.backgroundColor = UIColor.redColor()
					}
					//Add Error Message if not already added
					if !hourCheckSuccessErrorStatus {
						hourCheckSuccessErrorStatus = true
						if errorMessage == "" {
							errorMessage = hourCheckSuccessErrorMessage
						} else {
							errorMessage = "\(errorMessage)  \(hourCheckSuccessErrorMessage)"
						}
					}
				}
			} else if allShowsSegmentIndexArray[hourCounter] != 2 {
				let hourNotFilledInErrorMessage = "All Hour Fields must be filled in"
				if !hourNotFilledInErrorStatus {
					hourNotFilledInErrorStatus = true
					if errorMessage == "" {
						errorMessage = hourNotFilledInErrorMessage
					} else {
						errorMessage = "\(errorMessage)  \(hourNotFilledInErrorMessage)"
					}
				}
				switch hourCounter {
				case 0 :
					firstShowHour.backgroundColor = UIColor.orangeColor()
				case 1 :
					secondShowHour.backgroundColor = UIColor.orangeColor()
				case 2 :
					thirdShowHour.backgroundColor = UIColor.orangeColor()
				default :
					fourthShowHour.backgroundColor = UIColor.orangeColor()
				}
			}
			++hourCounter
		}
		
		//begin check to see if 1 hour between hour fields.
		let hour1HourApartErrorMessage = "All shows must be at least 1 hour after the previous show."
		
		func hourCheckSuccessErrorFunction() {
			if !hour1HourApartErrorStatus {
				hour1HourApartErrorStatus = true
				if errorMessage == "" {
					errorMessage = hour1HourApartErrorMessage
				} else {
					errorMessage = "\(errorMessage) \(hour1HourApartErrorMessage)"
				}
			}
		}
		
		//Check first show against second show
		if hour24s[0] != 24 && hour24s[1] != 24 && hour24s[1] - hour24s[0] <= 0 && firstShowHour.hidden == false && secondShowHour.hidden == false {
			hourCheckSuccessErrorFunction()
			if firstShowHour.backgroundColor == nil {
				firstShowHour.backgroundColor = UIColor.yellowColor()
			}
			if secondShowHour.backgroundColor == nil {
				secondShowHour.backgroundColor = UIColor.yellowColor()
			}
		}
		
		//Check second show agains thrid show
		if hour24s[1] != 24 && hour24s[2] != 24 && hour24s[2] - hour24s[1] <= 0 && secondShowHour.hidden == false && thirdShowHour.hidden == false {
			hourCheckSuccessErrorFunction()
			if secondShowHour.backgroundColor == nil {
				secondShowHour.backgroundColor = UIColor.yellowColor()
			}
			if thirdShowHour.backgroundColor == nil {
				thirdShowHour.backgroundColor = UIColor.yellowColor()
			}
		}
		
		//Check third show against fourth show
		if hour24s[2] != 24 && hour24s[3] != 24 && hour24s[3] - hour24s[2] <= 0 && thirdShowHour.hidden == false && fourthShowHour.hidden == false {
			hourCheckSuccessErrorFunction()
			if thirdShowHour.backgroundColor == nil {
				thirdShowHour.backgroundColor = UIColor.yellowColor()
			}
			if fourthShowHour.backgroundColor == nil {
				fourthShowHour.backgroundColor = UIColor.yellowColor()
			}
		}
		
		//Begin Checking Minute validity
		var minCounter = 0
		var minErrorMessageOrange = false
		var minErrorMessageRed = false
		for min in showMins {
			if let min = min {
				let minPassedValidity = minCheck(min)
				if minPassedValidity {
					
					switch minCounter {
					case 0:
						firstShowMinute.backgroundColor = nil
					case 1:
						secondShowMinute.backgroundColor = nil
					case 2:
						thirdShowMinute.backgroundColor = nil
					default:
						fourthShowMinute.backgroundColor = nil
					}
					
				} else if allShowsSegmentIndexArray[minCounter] != 2 {
					
					switch minCounter {
					case 0:
						firstShowMinute.backgroundColor = UIColor.redColor()
					case 1:
						secondShowMinute.backgroundColor = UIColor.redColor()
					case 2:
						thirdShowMinute.backgroundColor = UIColor.redColor()
					default:
						fourthShowMinute.backgroundColor = UIColor.redColor()
					}
					//create error message.
					let minCheckSuccessErrorMessage = "All Minute Fields must be between 0 and 59."
					if !minErrorMessageRed {
						minErrorMessageRed = true
						if errorMessage == "" {
							errorMessage = minCheckSuccessErrorMessage
						} else {
							errorMessage = "\(errorMessage)  \(minCheckSuccessErrorMessage)"
						}
					}
				}
			} else if allShowsSegmentIndexArray[minCounter] != 2 {
				//Min was blank
				switch minCounter {
				case 0:
					firstShowMinute.backgroundColor = UIColor.orangeColor()
				case 1:
					secondShowMinute.backgroundColor = UIColor.orangeColor()
				case 2:
					thirdShowMinute.backgroundColor = UIColor.orangeColor()
				default:
					fourthShowMinute.backgroundColor = UIColor.orangeColor()
				}
				//create error message.
				let minCheckSuccessErrorMessage = "All Minute Fields must be filled in."
				if !minErrorMessageOrange{
					minErrorMessageOrange = true
					if errorMessage == "" {
						errorMessage = minCheckSuccessErrorMessage
					} else {
						errorMessage = "\(errorMessage)  \(minCheckSuccessErrorMessage)"
					}
				}
			}
			++minCounter
		}
		
		//Format hour and minute and am/pm into a string so it can be uploaded to parse.
		var formatedFirstShowTime = ""
		var formatedSecondShowTime = ""
		var formatedThirdShowTime = ""
		var formatedFourthShowTime = ""
		
		func minuteFormatter(min: Int) -> String {
			switch min {
			case 0...9 :
				return "0\(min)"
			default:
				return "\(min)"
			}
		}
		
		if firstShowHour.backgroundColor == nil && firstShowMinute.backgroundColor == nil && firstShowSegmentedControl != 2 && !firstShowHour.hidden{
			var amPm = ""
			if firstShowSegmentedControl.selectedSegmentIndex == 0 {
				amPm = "am"
			} else {
				amPm = "pm"
			}
			formatedFirstShowTime = "\(firstShowHour.text):\(minuteFormatter(firstShowMinInt!)) \(amPm)"
		}
		
		if secondShowHour.backgroundColor == nil && secondShowMinute.backgroundColor == nil && secondShowSegmentedControl != 2 && !secondShowHour.hidden{
			var amPm = ""
			if secondShowSegmentedControl.selectedSegmentIndex == 0 {
				amPm = "am"
			} else {
				amPm = "pm"
			}
			formatedSecondShowTime = "\(secondShowHour.text):\(minuteFormatter(secondShowMinInt!)) \(amPm)"
		}
		
		if thirdShowHour.backgroundColor == nil && thirdShowMinute.backgroundColor == nil && thirdShowSegementedControl != 2 && !thirdShowHour.hidden{
			var amPm = ""
			if thirdShowSegementedControl.selectedSegmentIndex == 0 {
				amPm = "am"
			} else {
				amPm = "pm"
			}
			formatedThirdShowTime = "\(thirdShowHour.text):\(minuteFormatter(thirdShowMinInt!)) \(amPm)"
		}
		
		if fourthShowHour.backgroundColor == nil && fourthShowMinute.backgroundColor == nil && fourthShowSegmentedControl != 2 && !fourthShowHour.hidden {
			var amPm = ""
			if fourthShowSegmentedControl.selectedSegmentIndex == 0 {
				amPm = "am"
			} else {
				amPm = "pm"
			}
			formatedFourthShowTime = "\(fourthShowHour.text):\(minuteFormatter(fourthShowMinInt!)) \(amPm)"
		}
		
		//Capture the current selected row title for the picker to set what show to update.
		let selectedRowNumber = eventPicker.selectedRowInComponent(0)
		let selectedShowKey = keysForShows[selectedRowNumber]
		
		//if no errors utilize parse to update data on server.
		if errorMessage == "" {
			var query = PFQuery(className: "ShowTimes")
			query.getObjectInBackgroundWithId(selectedShowKey) {
				(ShowTimes: PFObject?, error: NSError?) -> Void in
				if error != nil {
					self.createAlert("Unable to update at this time.  Please try again later")
				} else if let ShowTimes = ShowTimes {
					println("succesfully pulled object")
					ShowTimes["firstShowTime"] = formatedFirstShowTime
					ShowTimes["secondShowTime"] = formatedSecondShowTime
					ShowTimes["thirdShowTime"] = formatedThirdShowTime
					ShowTimes["fourthShowTime"] = formatedFourthShowTime
					ShowTimes.saveInBackgroundWithBlock {
						(success: Bool, error: NSError?) -> Void in
						if success {
							self.updateActivityIndicator.stopAnimating()
							var bellSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bellSound", ofType: "mp3")!)
							AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
							AVAudioSession.sharedInstance().setActive(true, error: nil)
							
							var soundError: NSError?
							self.audioPlayer = AVAudioPlayer(contentsOfURL: bellSound, error: &soundError)
							self.audioPlayer.prepareToPlay()
							self.audioPlayer.play()
						} else {
							self.createAlert("Unable to update.  Try again later.")
						}
					}
				}
			}
		} else {
			//TODO: - Create alert
			createAlert(errorMessage)
		}
	}
	
	
	@IBAction func logout(sender: UIButton) {
		dismissViewControllerAnimated(true, completion: nil)
	}
	
}
