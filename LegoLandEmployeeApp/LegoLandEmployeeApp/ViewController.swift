//
//  ViewController.swift
//  LegoLandEmployeeApp
//
//  Created by David Fermer on 9/9/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {
	@IBOutlet weak var userName: UITextField!
	@IBOutlet weak var password: UITextField!
	@IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
	
	//Placeholder variables to be used to capture the user name and password field texts
	var usernameText = "Username"
	var passwordText = "Password"
	var role = "Not Logged In"
	//var user : PFUser?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		loginActivityIndicator.hidesWhenStopped = true
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewWillAppear(animated: Bool) {
		userName.text = ""
		password.text = ""
	}
	
	
	@IBAction func login(sender: UIButton) {
		loginActivityIndicator.startAnimating()
		usernameText = userName.text
		passwordText = password.text
		
		if usernameText == "" || passwordText == "" {
			//TODO: - create alert saying either user name or password was blank
			createAlert("Either the User Name or Password field was blank.  Please fill in both fields to continue.  If you don't know your user name or password please contact a Legoland Florida Admin")
			
		} else {
			//attempt to login
			let userLoggingIn: Void = PFUser.logInWithUsernameInBackground(usernameText, password:passwordText) {
				(user: PFUser?, error: NSError?) -> Void in
				if user != nil {
					// Do stuff after successful login.
					if let currentUser = PFUser.currentUser(){
						let role : String = currentUser.objectForKey("role")! as! String
						self.setUserRole(role)
						self.loginFunction(sender)
						self.loginActivityIndicator.stopAnimating()
					}
				} else {
					// The login failed.
					self.createAlert("Unseccussful Login.  Please check to see if your device is connected to the internet and that your username and password are correct.  If you don't know your user name or password please contact a Legoland Florida Admin.")
					self.loginActivityIndicator.stopAnimating()
				}
			}
		}
} // End login function



func setUserRole(role: String){
	self.role = role
}

func loginFunction(sender: UIButton){
	switch role {
	case "Manager":
		performSegueWithIdentifier("Manager", sender: sender)
	case "eventCoordinator":
		performSegueWithIdentifier("EventCoordinator", sender: sender)
	default:
		performSegueWithIdentifier("RideTechnician", sender: sender)
	}
}

func createAlert(errorMessage: String) {
	var alert = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
	presentViewController(alert, animated: true, completion: nil)
	alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
	
}

override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	switch role {
	case "Manager" :
		let ManagerVC = segue.destinationViewController as! ManagerViewController
		ManagerVC.userName = usernameText
	case "EventCoordinator":
		let EventVC = segue.destinationViewController as! EventCoordinatorViewController
		EventVC.userName = usernameText
	case "RideTechnician":
		let RideVC = segue.destinationViewController as! RideTechnicianViewController
		RideVC.userName = usernameText
	default:
		break
	}
}


}

