//
//  RideTechnicianViewController.swift
//  LegoLandEmployeeApp
//
//  Created by David Fermer on 9/10/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse
import AVFoundation

//MARK: - UIPickerViewDelegate
extension RideTechnicianViewController: UIPickerViewDelegate {
	func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
		return rideNames[row]
	}
}

//MARK: - UIPickerViewDataSource
extension RideTechnicianViewController: UIPickerViewDataSource {
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return numberOfRides
	}
}

class RideTechnicianViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
	@IBOutlet weak var ridesPicker: UIPickerView!
	@IBOutlet weak var waitTime: UITextField!
	@IBOutlet weak var rideStatus: UISwitch!
	
	var userName: String?
	var rideNames : [String] = []
	var objectIds : [String] = []
	var numberOfRides = 0
	
	//Audio Player to be used to play alert sounds
	var audioPlayer = AVAudioPlayer()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		var query = PFQuery(className:"Rides")
		query.whereKey("attractionType", equalTo:"ride")
		query.findObjectsInBackgroundWithBlock {
			(objects: [AnyObject]?, error: NSError?) -> Void in
			
			if error == nil {
    // The find succeeded.
				println("Successfully retrieved \(objects!.count) rides.")
    // Do something with the found objects
    if let objects = objects as? [PFObject] {
		self.numberOfRides = objects.count
		for object in objects {
			if let rideName = object["rideName"] as? String {
				self.rideNames.append(rideName)
				self.ridesPicker.reloadAllComponents()
				self.objectIds.append(object.objectId!)
			}
		}
    }
			} else {
    // Log details of the failure
				println("Error: \(error!) \(error!.userInfo!)")
			}
		}
		
		var tapGuesture = UITapGestureRecognizer(target: self, action: Selector("handleTapGuestre:"))
		view.addGestureRecognizer(tapGuesture)
	}
	
	func handleTapGuestre(tapGuesture: UITapGestureRecognizer) {
		self.view.endEditing(true)
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	func createAlert(errorMessage: String) {
		var alert = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
		presentViewController(alert, animated: true, completion: nil)
		alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
		
	}

	@IBAction func rideOnOffSwitchAction(sender: UISwitch) {
		if !sender.on {
			waitTime.text = "-"
		}
	}
	
	@IBAction func update(sender: UIButton) {
		if let waitTimeText = waitTime.text {
			var query = PFQuery(className: "Rides")
			let objectId = objectIds[ridesPicker.selectedRowInComponent(0)]
			println("\(objectId)")
			query.getObjectInBackgroundWithId("\(objectId)") {
				(Rides: PFObject?, error: NSError?) -> Void in
				if error != nil {
					self.createAlert("Unable to update at this time.  Please try again later.")
				} else if let Rides = Rides {
					println("successfully pulled object to be saved.")
					Rides["waitTime"] = "\(waitTimeText)"
					Rides["open"] = self.rideStatus.on
					Rides.saveInBackgroundWithBlock {
						(success: Bool, error: NSError?) -> Void in
						if success {
							var bellSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bellSound", ofType: "mp3")!)
							AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
							AVAudioSession.sharedInstance().setActive(true, error: nil)
							
							var soundError: NSError?
							self.audioPlayer = AVAudioPlayer(contentsOfURL: bellSound, error: &soundError)
							self.audioPlayer.prepareToPlay()
							self.audioPlayer.play()
						} else {
							self.createAlert("Check your internet Connection and try again.")						}
					}
				}
			}
		}
	}
	
	
	@IBAction func logOut(sender: UIButton) {
		dismissViewControllerAnimated(true, completion: nil)
	}
	
}

