//
//  ManagerViewController.swift
//  LegoLandEmployeeApp
//
//  Created by David Fermer on 9/10/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse
import AVFoundation

class ManagerViewController: UIViewController {
	@IBOutlet weak var dateLabel: UILabel!
	
	//Main Theme Park Hours Outlets
	@IBOutlet weak var themeParkOpenHour: UITextField!
	@IBOutlet weak var themeParkOpenMin: UITextField!
	@IBOutlet weak var themeParkOpenAmPm: UISegmentedControl!
	@IBOutlet weak var themeParkCloseHour: UITextField!
	@IBOutlet weak var themeParkCloseMin: UITextField!
	@IBOutlet weak var themeParkCloseAmPm: UISegmentedControl!
	
	//Water Park Hours Outlets
	@IBOutlet weak var waterParkOpenHour: UITextField!
	@IBOutlet weak var waterParkOpenMin: UITextField!
	@IBOutlet weak var waterParkOpenAmPm: UISegmentedControl!
	@IBOutlet weak var waterParkCloseHour: UITextField!
	@IBOutlet weak var waterParkCloseMin: UITextField!
	@IBOutlet weak var waterParkCloseAmPm: UISegmentedControl!
	
	//Activity Indicator
	@IBOutlet weak var updateActivityIndicator: UIActivityIndicatorView!
	
	//Audio Player to be used to play alert sounds
	var audioPlayer = AVAudioPlayer()
	
	//Currently logged in user
	var userName : String?
	
	//Park Open and Close Times as their complete strings
	var themeParkOpenText : String?
	var themeParkClosedText: String?
	var waterParkOpenText : String?
	var waterParkClosedText: String?
	
	//Park Open and Close Times as Integers waiting to be combined into Strings
	// Main Theme Park
	var themeParkOpenHourInt : Int?
	var themeParkOpenMinInt : Int?
	var themeParkCloseHourInt : Int?
	var themeParkCloseMinInt : Int?
	// Water Park
	var waterParkOpenHourInt : Int?
	var waterParkCloseHourInt : Int?
	var waterParkOpenMinInt : Int?
	var waterParkCloseMinInt : Int?
	
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		
		let date = NSDate()
		let format = NSDateFormatter()
		format.dateStyle = NSDateFormatterStyle.MediumStyle
		let dateString = format.stringFromDate(date)
		
		dateLabel.text = dateString
		updateActivityIndicator.hidesWhenStopped = true
		
		//Tap Guesture Recognizer
		var tapGuesture = UITapGestureRecognizer(target: self, action: Selector("handleTapGuestre:"))
		view.addGestureRecognizer(tapGuesture)
	}
	
	
	func handleTapGuestre(tapGuesture: UITapGestureRecognizer) {
		self.view.endEditing(true)
	}

	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	@IBAction func updateButton(sender: UIButton) {
		updateActivityIndicator.startAnimating()
		//capture outlet variables for park times and make into ints for purpose of checking validity
		//Hours
		themeParkOpenHourInt = themeParkOpenHour.text.toInt()
		themeParkCloseHourInt = themeParkCloseHour.text.toInt()
		waterParkOpenHourInt = waterParkOpenHour.text.toInt()
		waterParkCloseHourInt = waterParkCloseHour.text.toInt()
		let parkHours = [themeParkOpenHourInt, themeParkCloseHourInt, waterParkOpenHourInt, waterParkCloseHourInt]
		//24 hour Format.   Easier to ensure close is after opening
		var themeParkOpenHour24 = 0
		var themeParkCloseHour24 = 0
		var waterParkOpenHour24 = 0
		var waterParkCloseHour24 = 0
		
		//Mins
		themeParkOpenMinInt = themeParkOpenMin.text.toInt()
		themeParkCloseMinInt = themeParkCloseMin.text.toInt()
		waterParkOpenMinInt = waterParkOpenMin.text.toInt()
		waterParkCloseMinInt = waterParkCloseMin.text.toInt()
		let parkMins = [themeParkOpenMinInt, themeParkCloseMinInt, waterParkOpenMinInt, waterParkCloseMinInt]
		
		
		//The overal error message for anything wrong with the text fields
		var errorMessage = ""
		
		// This error message will be created if any hour field is missing.
		var hourEmptyFieldErrorMessage = ""
		//Park Hour Counter is used within loop to determine what iteration the loop is on
		var parkHourCounter = 0
		//Check Validity of hours
		//loop checks to see if each hour text field is in fact an Int and is between 1 and 12.
		for hour in parkHours {
			let hourErrorMessage = "Hour Must be between 1 and 12."
			if let hour = hour {
				let hourCheckPassedValidity = checkHour(hour)
				if hourCheckPassedValidity {
					
					//This error message is to be used whenever the close time is not at least 1 hour after the Theme Park open Time
					let errorMessageLengthTime = "close time must be at least 1 hour after the Theme Park open time."
					
					//Set background Color of text field back to normal if check passed
					switch parkHourCounter {
					case 0: // Theme Park Open Hour
						themeParkOpenHour.backgroundColor = nil
						//set up hours to 24 hour date format.
						if themeParkOpenAmPm.selectedSegmentIndex == 0 {
							themeParkOpenHour24 = hour
						} else {
							themeParkOpenHour24 = hour + 12
						}
					case 1: // Theme Park Close Hour
						themeParkCloseHour.backgroundColor = nil
						if themeParkCloseAmPm.selectedSegmentIndex == 0 {
							themeParkCloseHour24 = hour
						} else {
							themeParkCloseHour24 = hour + 12
						}
						if (themeParkCloseHour24 - themeParkOpenHour24) <= 0 {
							if errorMessage == "" {
								errorMessage = "The Theme Park \(errorMessageLengthTime)"
							} else {
								errorMessage = "\(errorMessage)  The Theme Park \(errorMessageLengthTime)"
							}
							themeParkOpenHour.backgroundColor = UIColor.yellowColor()
							themeParkCloseHour.backgroundColor = UIColor.yellowColor()
						}
					case 2: // Water Park Open Hour
						waterParkOpenHour.backgroundColor = nil
						if waterParkOpenAmPm.selectedSegmentIndex == 0 {
							waterParkOpenHour24 = hour
						} else {
							waterParkOpenHour24 = hour + 12
						}
					default: // Water Park Close Hour
						waterParkCloseHour.backgroundColor = nil
						if waterParkCloseAmPm.selectedSegmentIndex == 0 {
							waterParkCloseHour24 = hour
						} else {
							waterParkCloseHour24 = hour + 12
						}
						if (waterParkCloseHour24 - waterParkOpenHour24) <= 0 {
							if errorMessage == "" {
								errorMessage = "The Water Park \(errorMessageLengthTime)"
							} else {
								errorMessage = "\(errorMessage)  The Water Park \(errorMessageLengthTime)"
							}
							waterParkOpenHour.backgroundColor = UIColor.yellowColor()
							waterParkCloseHour.backgroundColor = UIColor.yellowColor()
						}
					}
					++parkHourCounter
				} else {
					switch parkHourCounter {
					case 0 : // Theme Park Open Hour
						++parkHourCounter
						if errorMessage == "" {
							errorMessage = "The Theme Park Open \(hourErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Theme Park Open \(hourErrorMessage)"
						}
						themeParkOpenHour.backgroundColor = UIColor.redColor()
					case 1: // Theme Park Close Hour
						++parkHourCounter
						if errorMessage == "" {
							errorMessage = "The Theme Park Close \(hourErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Theme Park Close \(hourErrorMessage)"
						}
						themeParkCloseHour.backgroundColor = UIColor.redColor()
					case 2: //Water Park Close Hour
						++parkHourCounter
						if errorMessage == "" {
							errorMessage = "The Water Park Open \(hourErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Water Park Open \(hourErrorMessage)"
						}
						waterParkOpenHour.backgroundColor = UIColor.redColor()
					default: // Water Park Open Hour
						++parkHourCounter
						if errorMessage == "" {
							errorMessage = "The Water Park Close \(hourErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Water Park Close \(hourErrorMessage)"
						}
						waterParkCloseHour.backgroundColor = UIColor.redColor()
					}
				}
			} else {
				if hourEmptyFieldErrorMessage == "" {
					hourEmptyFieldErrorMessage = "Please Ensure all hour Fields are filled out."
					if errorMessage == "" {
						errorMessage = "\(hourEmptyFieldErrorMessage)"
					} else {
						errorMessage = "\(errorMessage) \(hourEmptyFieldErrorMessage)"
					}
				}
				switch parkHourCounter {
				case 0:	themeParkOpenHour.backgroundColor = UIColor.orangeColor()
				case 1: themeParkCloseHour.backgroundColor = UIColor.orangeColor()
				case 2: waterParkOpenHour.backgroundColor = UIColor.orangeColor()
				default: waterParkCloseHour.backgroundColor = UIColor.orangeColor()
				}
				++parkHourCounter
			}
		}
		// End Hour Checks
		
		
		//Park Mins Counter is used within the loop to determin what iteration the loop is on.
		var parkMinsCounter = 0
		//To be created if there is an empty mins field
		var minsEmptyFieldErrorMessage = ""
		for mins in parkMins {
			if let mins = mins {
				let minsCheckPassedValidity = checkMin(mins)
				if minsCheckPassedValidity {
					//Switch is used to determine what mins text field is being updated
					switch parkMinsCounter {
					case 0:
						themeParkOpenMin.backgroundColor = nil
					case 1:
						themeParkCloseMin.backgroundColor = nil
					case 2:
						waterParkOpenMin.backgroundColor = nil
					default:
						waterParkCloseMin.backgroundColor = nil
					}
					//If check is passed reset backgournd color of selected mins text field to default and incrememnt parkMinsCounter
					++parkMinsCounter
				} else {
					let minsErrorMessage = "Mins must be between 0 and 59."
					switch parkMinsCounter {
					case 0:
						themeParkOpenMin.backgroundColor = UIColor.redColor()
						if errorMessage == "" {
							errorMessage = "The Theme Park Open \(minsErrorMessage)"
						} else {
							errorMessage = "\(errorMessage) The Theme Park Open \(minsErrorMessage)"
						}
						++parkMinsCounter
					case 1:
						themeParkCloseMin.backgroundColor = UIColor.redColor()
						if errorMessage == "" {
							errorMessage = "The Theme Park Close \(minsErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Theme Park Close \(minsErrorMessage)"
						}
						++parkMinsCounter
					case 2:
						waterParkOpenMin.backgroundColor = UIColor.redColor()
						if errorMessage == "" {
							errorMessage = "The Water Park Open \(minsErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Water Park Open \(minsErrorMessage)"
						}
						++parkMinsCounter
					default:
						waterParkCloseMin.backgroundColor = UIColor.redColor()
						if errorMessage == "" {
							errorMessage = "The Water Park Close \(minsErrorMessage)"
						} else {
							errorMessage = "\(errorMessage)  The Water Park Close \(minsErrorMessage)"
						}
						++parkMinsCounter
					}
				}
			} else {
				if minsEmptyFieldErrorMessage == "" {
					minsEmptyFieldErrorMessage = "Please Ensure all Minute Fields are filled out."
					if errorMessage == "" {
						errorMessage = "\(minsEmptyFieldErrorMessage)"
					} else {
						errorMessage = "\(errorMessage) \(minsEmptyFieldErrorMessage)"
					}
				}
				switch parkMinsCounter {
				case 0:	themeParkOpenMin.backgroundColor = UIColor.orangeColor()
				case 1: themeParkCloseMin.backgroundColor = UIColor.orangeColor()
				case 2: waterParkOpenMin.backgroundColor = UIColor.orangeColor()
				default: waterParkCloseMin.backgroundColor = UIColor.orangeColor()
				}
				++parkMinsCounter
			}
		}
		
		//create data to be passed to parse.  Will check all text field colors to ensure correct data format before building and sending to parse.  If incorrect data send alert to user.
		if themeParkOpenHour.backgroundColor == nil && themeParkCloseHour.backgroundColor == nil && themeParkOpenMin.backgroundColor == nil && themeParkCloseMin.backgroundColor == nil && waterParkOpenHour.backgroundColor == nil && waterParkCloseHour.backgroundColor == nil && waterParkOpenMin.backgroundColor == nil && waterParkCloseMin.backgroundColor == nil {
			//Build Time Formats into Strings
			let themeParkOpeningTime = "\(themeParkOpenHourInt!):\(minuteFormateCheck(themeParkOpenMinInt!)) \(themeParkOpenAmPm.titleForSegmentAtIndex(themeParkOpenAmPm.selectedSegmentIndex)!)"
			let themeParkClosingTime = "\(themeParkCloseHourInt!):\(minuteFormateCheck(themeParkCloseMinInt!)) \(themeParkCloseAmPm.titleForSegmentAtIndex(themeParkCloseAmPm.selectedSegmentIndex)!)"
			let waterParkOpeningTime = "\(waterParkOpenHourInt!):\(minuteFormateCheck(waterParkOpenMinInt!)) \(waterParkOpenAmPm.titleForSegmentAtIndex(waterParkOpenAmPm.selectedSegmentIndex)!)"
			let waterParkClosingTime = "\(waterParkCloseHourInt!):\(minuteFormateCheck(waterParkCloseMinInt!)) \(waterParkCloseAmPm.titleForSegmentAtIndex(waterParkCloseAmPm.selectedSegmentIndex)!)"
			
			//utilize parse to update data on server
			var query = PFQuery(className: "ParkData")
			query.getObjectInBackgroundWithId("a4NWw8LoE1") {
				(ParkData: PFObject?, error: NSError?) -> Void in
				if error != nil {
					println(error)
				} else if let ParkData = ParkData {
					ParkData["themeParkOpen"] = themeParkOpeningTime
					ParkData["themeParkClose"] = themeParkClosingTime
					ParkData["waterParkOpen"] = waterParkOpeningTime
					ParkData["waterParkClose"] = waterParkClosingTime
					ParkData["lastUpdatedBy"] = self.userName!
					ParkData.saveInBackgroundWithBlock {
						(success: Bool, error: NSError?) -> Void in
						if (success) {
							self.updateActivityIndicator.stopAnimating()
							var bellSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bellSound", ofType: "mp3")!)
							AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
							AVAudioSession.sharedInstance().setActive(true, error: nil)
							
							var soundError: NSError?
							self.audioPlayer = AVAudioPlayer(contentsOfURL: bellSound, error: &soundError)
							self.audioPlayer.prepareToPlay()
							self.audioPlayer.play()
						} else {
							self.createAlert("Unable to update try again later")
							self.updateActivityIndicator.stopAnimating()
						}
					}
				}
			}
		} else {
			createAlert(errorMessage)
			updateActivityIndicator.stopAnimating()
		}
	}
	
	
	func checkHour(hour: Int) -> Bool {
		switch hour {
		case 1...12 : return true
		default: return false
		}
	}
	
	func checkMin(min: Int) -> Bool {
		switch min {
		case 0...59 : return true
		default: return false
		}
	}
	
	func minuteFormateCheck(min: Int) -> String {
		if min > 9 {
			return "\(min)"
		} else {
			return "0\(min)"
		}
	}
	
	
	func createAlert(errorMessage: String) {
		var alert = UIAlertController(title: "Alert", message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
		presentViewController(alert, animated: true, completion: nil)
		alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
		
	}
	
	@IBAction func logout(sender: UIButton) {
		dismissViewControllerAnimated(true, completion: nil)
	}
}
