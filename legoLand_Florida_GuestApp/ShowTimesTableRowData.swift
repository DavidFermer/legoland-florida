//
//  ShowTimesTableRowData.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/16/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit

class ShowTimesTableRowData {
	let time: String
	var reminderImage : UIImage
	var reminderSet : Bool
	var show: String
	var reminderTitle : String

	
	init(reminderImage: UIImage, time: String, reminderSet: Bool, show: String) {
		self.reminderImage = reminderImage
		self.time = time
		self.reminderSet = reminderSet
		self.show = show
		self.reminderTitle = "\(show) at \(time)"
	}
}