//
//  CustomHeader.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/15/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit

class CustomHeader: UITableViewHeaderFooterView {

	
	@IBOutlet weak var legoBrickImage: UIImageView!
	@IBOutlet weak var headerLabel: UILabel!
	
	
}
