//
//  ShowsViewController.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/15/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse
import EventKit
import EventKitUI

// Identifiers in Global Scope
let cellIdentifier = "cellIdentfier";
let headerIdentifier = "headerReuse";

// MARK: - NSDate extension
//Allows for conversion of strings to NSDates
extension NSDate {
	convenience
	init(dateString:String) {
		let dateStringFormatter = NSDateFormatter()
		dateStringFormatter.dateFormat = "yyyy-MM-dd"
		dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
		let d = dateStringFormatter.dateFromString(dateString)!
		self.init(timeInterval:0, sinceDate:d)
	}
}

//MARK: - UITableViewDataSource
extension ShowsViewController: UITableViewDataSource {
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return showNames.count
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return showTimes[section].count
	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let header = showsTable.dequeueReusableHeaderFooterViewWithIdentifier(headerIdentifier) as! CustomHeader
		
		if section % 2 == 0 {
			if let red = redBrick {
				header.legoBrickImage.image = red
			}
		} else {
			if let blue = blueBrick {
				header.legoBrickImage.image = blue
			}
		}
		
		header.headerLabel.text = showNames[section]
		
		return header
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 100.0
	}
	
	func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 15.0
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let cell = showsTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ShowTimesTableViewCell
		
		cell.setBackgroundImageForCell(indexPath.section)
		
		let tagFirstDigit = indexPath.section
		let tagSecondDigit = indexPath.row
		let tagAsString = "\(tagFirstDigit)\(tagSecondDigit)"
		
		cell.reminderButton.setImage(showTimes[indexPath.section][indexPath.row].reminderImage, forState: .Normal)
		
		cell.reminderButton.tag = Int(tagAsString)!
		
		cell.timeLabel.text = showTimes[indexPath.section][indexPath.row].time
		return cell
	}
	
}

//MARK: - ShowsViewController
class ShowsViewController: UIViewController, UITableViewDelegate {
	@IBOutlet weak var showsTable: UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	
	
	//Table Data
	var showNames : [String] = []
	var showTimes : [[ShowTimesTableRowData]] = [[]]
	
	var redBrick : UIImage?
	var blueBrick : UIImage?
	var darkAlarm : UIImage?
	var lightAlarm : UIImage?
	var selectedReminder: [Int] = []
	
	var alarmsOnTagArrray : [Int] = []
	
	// Reminder related Variables
	let eventStore = EKEventStore()
	
	//MARK: - ViewDidLoad
	override func viewDidLoad() {
		activityIndicator.hidesWhenStopped = true
		activityIndicator.startAnimating()
		
		redBrick = UIImage(named: "redLegoBrick")
		blueBrick = UIImage(named: "blueLegoBrick")
		darkAlarm = UIImage(named: "reminder")
		lightAlarm = UIImage(named: "reminderLight")
		
		// Register the NIB file with our tableView that way we can dequeue our own custom header
		super.viewDidLoad()
		
		showsTable.registerNib(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier);
		showsTable.backgroundView = nil
		showsTable.backgroundColor = UIColor.clearColor()
		let query = PFQuery(className:"ShowTimes")
		query.whereKey("type", equalTo:"show")
		query.findObjectsInBackgroundWithBlock {
			(objects: [AnyObject]?, error: NSError?) -> Void in
			
			if error == nil {
				if let objects = objects as? [PFObject] {
					var firstRun = true
					for object in objects {
						if let showName: String = object["showName"] as? String {
							self.showNames.append(showName)
							
							
							var currentShowTimes: [ShowTimesTableRowData] = []
							
							if let firstShow: String = object["firstShowTime"] as? String {
								currentShowTimes.append(ShowTimesTableRowData(reminderImage: self.darkAlarm!, time: firstShow, reminderSet: false, show: showName))
							} else {
								currentShowTimes.append(ShowTimesTableRowData(reminderImage: self.darkAlarm!, time: "No Show Scheduled", reminderSet: false, show: showName))
							}
							
							if let secondShow: String = object["secondShowTime"] as? String {
								if secondShow != "" {
									currentShowTimes.append(ShowTimesTableRowData(reminderImage: self.darkAlarm!, time: secondShow, reminderSet: false, show: showName))
								}
							}
							
							if let thirdShow: String = object["thirdShowTime"] as? String {
								if thirdShow != "" {
									currentShowTimes.append(ShowTimesTableRowData(reminderImage: self.darkAlarm!, time: thirdShow, reminderSet: false, show: showName))
								}
							}
							
							if let fourthShow: String = object["fourthShowTime"] as? String {
								if fourthShow != "" {
									currentShowTimes.append(ShowTimesTableRowData(reminderImage: self.darkAlarm!, time: fourthShow, reminderSet: false, show: showName))
								}
							}
							if firstRun{
								self.showTimes[0] = currentShowTimes
								firstRun = false
							} else {
								self.showTimes.append(currentShowTimes)
							}
						}
						self.activityIndicator.stopAnimating()
					}
					if EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder) == .Authorized {
						let predicate = self.eventStore.predicateForRemindersInCalendars([])
						self.eventStore.fetchRemindersMatchingPredicate(predicate) { reminders in
							for reminder in reminders! {
								for show in self.showTimes {
									for showTime in show {
										if showTime.reminderTitle == reminder.title {
											showTime.reminderImage = self.lightAlarm!
											dispatch_async(dispatch_get_main_queue(), { () -> Void in
												self.showsTable.reloadData()
											})
										}
									}
								}
							}}
					}
					self.showsTable.reloadData()
				}
			} else {
				self.activityIndicator.stopAnimating()
				let alert = UIAlertController(title: "Alert", message: "Please check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
				self.presentViewController(alert, animated: true, completion: nil)
				alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func reminderButton(sender: UIButton) {
		//Check for access to the calendar
		let status = EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)
		switch status {
		case .NotDetermined:
			print("Access not determined")
			eventStore.requestAccessToEntityType(EKEntityType.Reminder, completion: handler)
		case .Authorized:
			print("Access Authorized")
			
			let tagAsString = "\(sender.tag)"
			
			selectedReminder = []
			
			for c in tagAsString.characters {
				if tagAsString.characters.count == 1 {
					selectedReminder.append(0)
				}
				let characterAsString = "\(c)"
				let characterAsInt = Int(characterAsString)
				selectedReminder.append(characterAsInt!)
				
			}
			
			let currentDateTime = NSDate()
			let format = NSDateFormatter()
			format.dateStyle = NSDateFormatterStyle.ShortStyle
			let dateString = format.stringFromDate(currentDateTime)
			let formattedDateString = dateString
			let formattedDateArray = formattedDateString.componentsSeparatedByString("/")
			
			
			let timeParts = showTimes[selectedReminder[0]][selectedReminder[1]].time
			let timeAmPmArray = timeParts.componentsSeparatedByString(" ")
			let amPM = timeAmPmArray[1]
			let hourMinsArray = timeAmPmArray[0].componentsSeparatedByString(":")
			
			let year = Int(formattedDateArray[2])! + 2000
			let month = formattedDateArray[0]
			let day = formattedDateArray[1]
			var hour = Int(hourMinsArray[0])
			let min = Int(hourMinsArray[1])
			
			if amPM == "pm" && hour! < 12 {
				hour = hour! + 12
			} else if hour! == 12 {
				hour = 0
			}
			
			let timeInterval: NSTimeInterval = Double((hour!*60*60) + (min!*60)) - Double(30*60)
			let alertDay = NSDate(dateString: "\(year)-\(month)-\(day)")
			let alertTime = alertDay.dateByAddingTimeInterval(timeInterval)
			
			if !showTimes[selectedReminder[0]][selectedReminder[1]].reminderSet {
				var reminder = EKReminder(eventStore: eventStore)
				reminder.title = showTimes[selectedReminder[0]][selectedReminder[1]].reminderTitle
				reminder.calendar = eventStore.defaultCalendarForNewReminders()
				reminder.addAlarm(EKAlarm(absoluteDate: alertTime))
				showTimes[selectedReminder[0]][selectedReminder[1]].reminderImage = lightAlarm!
				showTimes[selectedReminder[0]][selectedReminder[1]].reminderSet = true
				do {
					try eventStore.saveReminder(reminder, commit: true)
				} catch _ {
				}
			} else {
				var predicate = eventStore.predicateForRemindersInCalendars([])
				eventStore.fetchRemindersMatchingPredicate(predicate) { reminders in
					for reminder in reminders! {
						print(reminder.description)
						if reminder.title == self.showTimes[self.selectedReminder[0]][self.selectedReminder[1]].reminderTitle {
							do {
								try self.eventStore.removeReminder(reminder , commit: true)
							} catch _ {
							}
							dispatch_async(dispatch_get_main_queue(), { () -> Void in
								self.showsTable.reloadData()
							})
						}
					}}
				
				
				showTimes[selectedReminder[0]][selectedReminder[1]].reminderImage = darkAlarm!
				showTimes[selectedReminder[0]][selectedReminder[1]].reminderSet = false
			}
			self.showsTable.reloadData()
			
		default:
			break
		}
		
	}
	
	private func handler (granted: Bool, error: NSError!) {
		if let error = error {
			let alert =	UIAlertController(title: "Alert", message: "Requesting Access To your calendars has failed With Error: \(error)", preferredStyle: .Alert )
			let alertAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
			alert.addAction(alertAction)
			presentViewController(alert, animated: true, completion: nil)
			return
		}
		
		if granted == false {
			let alert = UIAlertController(title: "Alert", message: "We can not set reminders for you until you give access to the calendars.", preferredStyle: UIAlertControllerStyle.Alert)
			self.presentViewController(alert, animated: true, completion: nil)
			alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
		}
		
	}
	
}
