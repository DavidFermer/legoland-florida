//
//  ShowTimesTableViewCell.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/15/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit

class ShowTimesTableViewCell: UITableViewCell {

	@IBOutlet weak var backgroundImage: UIImageView!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var reminderButton: UIButton!
	
	var reminderLight = false
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setBackgroundImageForCell(section: Int) {
		if section % 2 == 0 {
			backgroundImage.image = UIImage(named: "redLegoBrickTableViewCell")
		} else {
			backgroundImage.image = UIImage(named: "blueLegoBrickTableCell")
		}
	}
	
	func setReminderButtonImage() {
		if reminderLight {
			reminderButton.setImage(UIImage(named: "reminderLight"), forState: .Normal)
		} else if timeLabel.text == "No Show Scheduled" {
			reminderButton.hidden = true
		} else  {
			reminderButton.setImage(UIImage(named: "reminder"), forState: .Normal)
		}
	}

}
