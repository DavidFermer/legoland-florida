//
//  SecondViewController.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/8/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse

class SecondViewController: UIViewController {
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var themeParkTimes: UILabel!
	@IBOutlet weak var waterParkTime: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		activityIndicator.hidesWhenStopped = true
		activityIndicator.stopAnimating()
		
		//Retrive Data from parse
		let query = PFQuery(className:"ParkData")
		query.getObjectInBackgroundWithId("a4NWw8LoE1") {
			(ParkData: PFObject?, error: NSError?) -> Void in
			if error == nil && ParkData != nil {
				if let ParkData = ParkData{
					let themeParkOpen: String = ParkData["themeParkOpen"] as! String
					let themeParkClosed: String = ParkData["themeParkClose"] as! String
					let waterParkOpen: String = ParkData["waterParkOpen"] as! String
					let waterParkClosed: String = ParkData["waterParkClose"] as! String
					self.themeParkTimes.text = "\(themeParkOpen) - \(themeParkClosed)"
					self.waterParkTime.text = "\(waterParkOpen) - \(waterParkClosed)"
				}
				self.activityIndicator.stopAnimating()
			} else {
				let alert = UIAlertController(title: "Alert", message: "Please check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
				self.presentViewController(alert, animated: true, completion: nil)
				alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
				self.activityIndicator.stopAnimating()
				
			}
		}
		
		
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
}

