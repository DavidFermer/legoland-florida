//
//  RideInfo.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/19/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import Foundation
import Parse

class RideInfo {
	var rideName: String
	var details: String
	var waitTime: String
	var latLong: PFGeoPoint
	var heightRequirementWithParent: String
	var heightRequirementWithoutParent: String
	var open: Bool
	
	init(rideName: String, details: String, waitTime: String, latLong: PFGeoPoint, heightRequirementWithParent: String, heightRequirementWitouthParent: String, open: Bool) {
		self.rideName = rideName
		self.details = details
		self.waitTime = waitTime
		self.latLong = latLong
		self.heightRequirementWithoutParent = heightRequirementWitouthParent
		self.heightRequirementWithParent = heightRequirementWithParent
		self.open = open
	}
}