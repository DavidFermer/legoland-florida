//
//  MapOverlayView.swift
//  legoLand_Florida_GuestApp
//
//  Created by David Fermer on 9/21/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import Foundation
import MapKit

class MapOverlayView: MKOverlayRenderer {
	
	var overlayImage: UIImage
	
	init (overlay: MapOverlayCustomClass, overlayImage: UIImage) {
		self.overlayImage = overlayImage
		super.init(overlay: overlay)
	}
	
	
	override func drawMapRect(mapRect: MKMapRect, zoomScale: MKZoomScale, inContext context: CGContext) {
		let imageReference = overlayImage.CGImage
		
		let theMapRect = overlay.boundingMapRect
		let theRect = rectForMapRect(theMapRect)
		
		CGContextScaleCTM(context, 1.0, -1.0)
		CGContextTranslateCTM(context, 0.0, -theRect.size.height)
		CGContextDrawImage(context, theRect, imageReference)
	}
	
}