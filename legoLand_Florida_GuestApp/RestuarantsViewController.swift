//
//  RestuarantsViewController.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/18/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse

//Global Identifiers
let cellIdentfier = "cellIdentfier"
let headerIdentifer = "headerReuse"

//MARK: - UITableViewDataSource
extension RestuarantsViewController: UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return restuarants.count
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return restuarants[section].menu.count
	}
	
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let header = restuarantTableView.dequeueReusableHeaderFooterViewWithIdentifier(headerIdentifier) as! CustomHeader
		
		if section % 2 == 0 {
			if let red = redBrick {
				header.legoBrickImage?.image = red
			}
		} else {
			if let blue = blueBrick {
				header.legoBrickImage?.image = blue
			}
		}
		
		header.headerLabel.text = restuarants[section].restuarantName
		
		return header
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 100.0
	}
	
	func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 15.0
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = restuarantTableView.dequeueReusableCellWithIdentifier(cellIdentfier) as! CustomCell
		
		cell.setBackgroundImageForCell(indexPath.section)
		
		cell.itemLabel.text = restuarants[indexPath.section].menu[indexPath.row]
		return cell
	}
}

class RestuarantsViewController: UIViewController {
	@IBOutlet weak var restuarantTableView: UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	var restuarants : [Restuarant] = []
	
	//Brick Images for table section headers
	var redBrick : UIImage?
	var blueBrick : UIImage?
	var redCellBrick: UIImage?
	var blueCellBrick: UIImage?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		activityIndicator.hidesWhenStopped = true
		activityIndicator.startAnimating()
		redBrick = UIImage(named: "redLegoBrick")
		blueBrick = UIImage(named: "blueLegoBrick")
		redCellBrick = UIImage(named: "redLegoBrickTableViewCell")
		blueCellBrick = UIImage(named: "blueLegoBrickTableViewCell")
		restuarantTableView.registerNib(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier);
		restuarantTableView.backgroundView = nil
		restuarantTableView.backgroundColor = UIColor.clearColor()
		
		//set up data source from parse
		var query = PFQuery(className:"Restuarants")
		query.whereKey("restuarantType", equalTo:"restuarant")
		query.findObjectsInBackgroundWithBlock {
			(objects: [AnyObject]?, error: NSError?) -> Void in
			
			if error == nil {
				if let objects = objects as? [PFObject] {
					var firstRun = true
					for object in objects {
						var restuarant = ""
						var menue : [String] = []
						
						if let restuarantName: String = object["restuarantName"] as? String {
							restuarant = restuarantName
						}
						if let menu : NSArray = object["menu"] as? NSArray {
							menue = menu as! [String]
						}
						self.restuarants.append(Restuarant(restuarantName: restuarant, menu: menue))
						dispatch_async(dispatch_get_main_queue(), { () -> Void in
							self.restuarantTableView.reloadData()
							self.activityIndicator.stopAnimating()
						})
					}
				}
			} else {
				
				func createAlert(){
					self.activityIndicator.stopAnimating()
					let alert = UIAlertController(title: "Alert", message: "Please check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
					self.presentViewController(alert, animated: true, completion: nil)
					alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
				}
			}
		}
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
}
