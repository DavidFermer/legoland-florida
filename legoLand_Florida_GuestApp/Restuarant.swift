//
//  Restuarant.swift
//  legoLand_Florida_GuestApp
//
//  Created by David Fermer on 9/20/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import Foundation

class Restuarant {
	var restuarantName: String
	var menu: [String]
	
	init(restuarantName: String, menu: [String]) {
		self.restuarantName = restuarantName
		self.menu = menu
	}
}