//
//  FirstViewController.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/8/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import Parse
import MapKit
import CoreLocation

//Global Identifier
let annotationId = "annotationReuse"

// MARK: - CLLOcationManagerDelegate
extension FirstViewController: CLLocationManagerDelegate {
	func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if status == .AuthorizedWhenInUse {
			locationManager.startUpdatingLocation()
		}
	}
	
	func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
		createAlert("There was an error updating your location.")
	}
}


// MARK: - MKMapViewDelegate
extension FirstViewController: MKMapViewDelegate {
	func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
		if CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse {
			return
		}
		
		let userLocationIsItVisible = mapView.userLocationVisible
		
		if userLocationIsItVisible {
			let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
			let region = MKCoordinateRegion(center: userLocation.coordinate, span: span)
			
			mapView.setRegion(region, animated: true)
		}
	}
	
	func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
		
		if annotation is MKUserLocation {
			return nil
		}
		
		var pin = MKAnnotationView()
		pin.annotation = annotation
		
		//Time Label
		var label = UILabel(frame: CGRectMake(0, 0, 20, 15))
		label.center = CGPointMake(12, 15)
		label.textAlignment = NSTextAlignment.Center
		label.textColor = UIColor.whiteColor()
		
		for var i = 0; i < annotations.count; i++ {
			if annotations[i].title == annotation.title {
				pin.tag = i
				if customPins[i].open{
					pin.image = UIImage(named: "blueCustomPinImage")
				} else {
				pin.image = UIImage(named: "customPinImage")
				}
				
				label.text = customPins[i].waitTime
	
				
			}
		}
		pin.canShowCallout = false
		pin.addSubview(label)
		
		
		return pin
	}
	
	func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
		if overlay is ParkMapOverlay {
			let overlayImage = UIImage(named: "overlayImage")
			let overlayView = ParkMapOverlayView(overlay: overlay, overlayImage: overlayImage!)
			
			return overlayView
		}
		return nil
	}
	
	func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
		//descover which annotation was selected.
		for ride in customPins {
			if view.annotation!.title! == ride.rideName {
				//Set the detail view
				rideInformationView.hidden = false
				rideName.text = ride.rideName
				heightWithAdult.text = ride.heightRequirementWithParent
				heightWithoutAdult.text = ride.heightRequirementWithoutParent
				rideDetailText.text = ride.details
				rideDetailText.textColor = UIColor(red: 235.0, green: 255.0, blue: 236.0, alpha: 1.0)
			}
		}
	}
	
	func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
		rideInformationView.hidden = true
	}
	
}

// MARK: - FirstViewController
class FirstViewController: UIViewController{
	
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var rideInformationView: UIView!
	@IBOutlet weak var rideName: UILabel!
	@IBOutlet weak var heightWithAdult: UILabel!
	@IBOutlet weak var heightWithoutAdult: UILabel!
	@IBOutlet weak var rideDetailText: UITextView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!

	let locationManager = CLLocationManager();
	
	var customPins : [RideInfo] = []
	var annotations: [MKAnnotation] = []
	var park = ParkData(filename: "overlayCoordinates")
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		activityIndicator.hidesWhenStopped = true
		activityIndicator.startAnimating()
		
		//set up overlay
		let latDelta = park.overlayTopLeftCoordinate.latitude - park.overlayBottomRightCoordinate.latitude
		let span = MKCoordinateSpanMake(fabs(latDelta), 0.0)
		let region = MKCoordinateRegionMake(park.midCoordinate, span)
		
		mapView.region = region
		
		let overlay = ParkMapOverlay(park: park)
		mapView.addOverlay(overlay)
		
		//Determine authorization to use map.
		let status = CLLocationManager.authorizationStatus()
		
		switch status {
		case .NotDetermined:
			locationManager.requestWhenInUseAuthorization()
		case .AuthorizedWhenInUse:
			locationManager.startUpdatingLocation()
			
		case .Denied:
			createAlert("If you would like to see your current location turn on location services for this app in iPhone settings.")
			break
		case .Restricted:
			// Tell the user that thier parent has not allowed them to use location services
			createAlert("Your Parent/Guardian has not allowed location services for this app.")
			break
		default:
			createAlert("Location Services have been turned off for this app.  You will not be able to see your current location.")
			break
		}
		
		
		mapView.showsUserLocation = true
		mapView.mapType = .Standard
		
		//Begin retrieving data from parse
		PFUser.logInWithUsernameInBackground("parkGuest", password:"1234") {
			(user: PFUser?, error: NSError?) -> Void in
			if user != nil {
				let query = PFQuery(className:"Rides")
				query.whereKey("attractionType", equalTo:"ride")
				query.findObjectsInBackgroundWithBlock {
					(objects: [AnyObject]?, error: NSError?) -> Void in
					
					if error == nil {
						// The find succeeded.
						if let objects = objects as? [PFObject] {
							for object in objects {
								let rideName: String = object["rideName"] as! String
								let details: String = object["writeUp"] as! String
								let waitTime: String = object["waitTime"] as! String
								let latLong: PFGeoPoint = object["coordinates"] as! PFGeoPoint
								let heightRequirementWithParent: String = object["heightRequirementWithAdult"] as! String
								let heightRequirementWithoutParent: String = object["heightRequirement"] as! String
								let open: Bool = object["open"] as! Bool
								
								let ride = RideInfo(rideName: rideName, details: details, waitTime: waitTime, latLong: latLong, heightRequirementWithParent: heightRequirementWithParent, heightRequirementWitouthParent: heightRequirementWithoutParent, open: open)
								self.customPins.append(ride)
								self.activityIndicator.stopAnimating()
							}
							
							for pin in self.customPins {
								let annotation = MKPointAnnotation();
								annotation.coordinate = CLLocationCoordinate2D(latitude: pin.latLong.latitude, longitude: pin.latLong.longitude)
								annotation.title = pin.rideName
								annotation.subtitle = pin.waitTime
								dispatch_async(dispatch_get_main_queue(), { () -> Void in
									self.mapView.addAnnotation(annotation)
									self.annotations.append(annotation)
								})
							}
						}
					} else {
						// Log details of the failure
						self.activityIndicator.stopAnimating()
						self.createAlert("Please check your internet connection")
					}
				}
			} else {
				// The login failed. Check error to see why.
				self.activityIndicator.stopAnimating()
				self.createAlert("Please check your internet connection")
			}
		}
		let mapCamera = MKMapCamera()
		//set the initial region for viewing:
		let center = CLLocationCoordinate2D(latitude: 27.989626, longitude: -81.691179)
		//set mapView rotation
		mapCamera.heading = CLLocationDirection(90.0)
		mapCamera.centerCoordinate = center
		mapView.setCamera(mapCamera, animated: false)
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	func createAlert(alertMessage: String){
		let alert = UIAlertController(title: "Alert", message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
		presentViewController(alert, animated: true, completion: nil)
		alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
	}
	
}

