//
//  ParkMapOverlay.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/22/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import Foundation
import MapKit

class ParkMapOverlay: NSObject, MKOverlay {
	var coordinate: CLLocationCoordinate2D
	var boundingMapRect: MKMapRect
	
	init (park: ParkData) {
		boundingMapRect = park.overlayBoundingMapRect
		coordinate = park.midCoordinate
	}
}