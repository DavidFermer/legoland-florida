//
//  CustomCell.swift
//  Legoland_Florida
//
//  Created by David Fermer on 9/15/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

	@IBOutlet weak var cellImageBackground: UIImageView!
	@IBOutlet weak var itemLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setBackgroundImageForCell(section: Int) {
		if section % 2 == 0 {
			cellImageBackground.image = UIImage(named: "redLegoBrickTableViewCell")
		} else {
			cellImageBackground.image = UIImage(named: "blueLegoBrickTableCell")
		}
	}


}
