//
//  MapOverlayCustomClass.swift
//  legoLand_Florida_GuestApp
//
//  Created by David Fermer on 9/21/15.
//  Copyright (c) 2015 David Fermer. All rights reserved.
//

import UIKit
import MapKit

class MapOverlayCustomClass: NSObject, MKOverlay {
	
	var coordinate:  CLLocationCoordinate2D
	var boundingMapRect: MKMapRect
	
	init(coordinate: CLLocationCoordinate2D, boundingMapRect: MKMapRect) {
		self.coordinate = coordinate
		self.boundingMapRect = boundingMapRect
	}
	
	
}